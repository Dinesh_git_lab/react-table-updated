import { Root } from "./components/Root";

export const App = () => <Root />;

export * from "./components/Root";
