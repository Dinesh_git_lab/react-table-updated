import React, { useState } from "react";

import Pagination from "./Pagination";
import Table from "./Table";
import TableHeader from "./TableHeader";

export const Root = ({ initialCount = 0 }) => {
  const [count, setCount] = useState(initialCount);
  return (
    <div style={{ color: "black" }}>
      <h1
        onClick={() => {
          setCount((prev) => prev + 1);
        }}
      >
        {count}
      </h1>
      <TableHeader />
      <Table />
      <Pagination />
    </div>
  );
};
